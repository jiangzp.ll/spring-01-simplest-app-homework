package com.twuc.webApp.controller;

import com.twuc.webApp.util.OperateEnum;
import com.twuc.webApp.util.TableUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tables")
public class TableController {

    @GetMapping("/plus")
    String getAdditionTable() {
        return TableUtil.generateTable(OperateEnum.PLUS);
    }

    @GetMapping("/multiply")
    String getMultiplicationTable() {
        return TableUtil.generateTable(OperateEnum.MULTIFY);
    }
}
