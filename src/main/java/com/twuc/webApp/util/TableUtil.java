package com.twuc.webApp.util;

import java.util.stream.Stream;

public class TableUtil {

    public static String generateTable(OperateEnum operate){
        StringBuilder result = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                switch (operate) {
                    case PLUS:
                        result.append(i).append("+").append(j).append("=").append(i + j).append(" ");
                        break;

                    case MULTIFY:
                        result.append(i).append("*").append(j).append("=").append(i * j).append(" ");
                        break;
                }
            }
            result.append("<br>");
        }
        return result.toString();
    }
}
