package com.twuc.webApp.controller;

import com.twuc.webApp.util.OperateEnum;
import com.twuc.webApp.util.TableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TableControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_and_addition_table_when_visit_addition_uri_given_a_get_request() throws Exception {
        mockMvc.perform(get("/api/tables/plus"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string(TableUtil.generateTable(OperateEnum.PLUS)));
    }

    @Test
    void should_return_200_and_multiplication_table_when_visit_multiplication_uri_given_a_get_request() throws Exception {
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string(TableUtil.generateTable(OperateEnum.MULTIFY)));
    }
}
